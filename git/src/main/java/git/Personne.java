package git;
import java.util.List;

public class Personne {
    private List<String> prenom;
    private String nom;
    private int age;
    private int sexe;

    public Personne(List<String> prenom,String nom,int age,int sexe){
        this.prenom = prenom;
        this.nom = nom;
        if(age >= 0)
            this.age = age;
        if(sexe == 1 || sexe == 2)
            this.sexe = sexe;
    }

    public void recherche(){

    }

    public String toString(boolean presentation, boolean sens){
        String s = "";
        
        if(sexe == 1){
            s = s + "M. ";
        }
        else if(sexe == 2){
            s = s + "Mme. ";
        }

        for(String p : prenom){
            if(presentation)
                s = s + p.charAt(0)+". ";
            else
                s = s + p + " ";
        }

        if(sens)
            s = nom + " " + s;
        else
            s = s + " " + nom;

        s = s +  " " + age + " ans";
        return s;
    }

}
